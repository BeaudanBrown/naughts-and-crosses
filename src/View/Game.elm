module View.Game
    exposing
        ( Msg(..)
        , render
        , renderBoard
        )

import Data.Board.Board as Board exposing (Board)
import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.Board.CellPtr as CellPtr exposing (CellPtr)
import Data.CellState exposing (CellState(..))
import Data.Game as Game exposing (BoardState(..), Game, Player)
import Data.PixelWidth exposing (PixelWidth)
import Data.PositiveInt as PositiveInt
import Data.StrictlyPositiveInt as StrictlyPositiveInt
import Html exposing (Html)
import Html.Attributes
import Html.Events exposing (onClick)


type Msg
    = OnClickMsg CellPtr


type alias Options =
    { pixelWidth : PixelWidth
    , playerToMove : Player
    , cellPtr : CellPtr
    , playableBoardPtr : CellPtr
    }


render : PixelWidth -> Game -> Html Msg
render pixelWidth game =
    let
        pixelWidthInt =
            PositiveInt.toInt pixelWidth

        options =
            { pixelWidth = pixelWidth
            , playerToMove = Game.getPlayerToMove game
            , cellPtr = CellPtr.empty
            , playableBoardPtr = Game.getPlayableBoard game
            }
    in
    Html.div
        [ Html.Attributes.style
            [ ( "position", "relative" )
            , ( "width", toString pixelWidthInt ++ "px" )
            , ( "height", toString pixelWidthInt ++ "px" )
            ]
        ]
        [ renderBoard options (Game.getBoard game) ]


renderBoard : Options -> Board CellState -> Html Msg
renderBoard options board =
    let
        sizeInt =
            Board.getSize board
                |> StrictlyPositiveInt.toInt

        childPixelWidth =
            PositiveInt.map (\x -> x // sizeInt) options.pixelWidth
                |> Maybe.withDefault PositiveInt.zero

        newOptions =
            { options | pixelWidth = childPixelWidth }

        winner =
            Game.getBoardWinner board
    in
    Html.div
        []
        (case winner of
            Win player ->
                [ renderCellState options ( CellCrds.origin, Game.playerToCellState player ) ]

            _ ->
                Board.toList board
                    |> List.map (renderCellState newOptions)
        )


renderCellState : Options -> ( CellCrds, CellState ) -> Html Msg
renderCellState options ( crds, cellState ) =
    let
        newOptions =
            { options | cellPtr = CellPtr.append crds options.cellPtr }
    in
    case cellState of
        Blank ->
            renderCellWrapper options crds (renderBlank newOptions)

        Naught ->
            renderCellWrapper options crds (Html.text "O")

        Cross ->
            renderCellWrapper options crds (Html.text "X")

        CellBoard b ->
            renderCellWrapper options crds (renderBoard newOptions b)


renderBlank : Options -> Html Msg
renderBlank options =
    let
        canPlay =
            CellPtr.isPredecessor options.playableBoardPtr options.cellPtr

        divList =
            if canPlay then
                [ Html.Attributes.style
                    [ ( "position", "absolute" )
                    , ( "width", "100%" )
                    , ( "height", "100%" )
                    ]
                , onClick (OnClickMsg options.cellPtr)
                ]
            else
                [ Html.Attributes.style
                    [ ( "position", "absolute" )
                    , ( "width", "100%" )
                    , ( "height", "100%" )
                    ]
                ]
    in
    Html.div
        divList
        []


renderCellWrapper : Options -> CellCrds -> Html Msg -> Html Msg
renderCellWrapper options crds child =
    let
        pixelWidthInt =
            PositiveInt.toInt options.pixelWidth

        ( x, y ) =
            CellCrds.toInts crds

        canPlay =
            options.playableBoardPtr == options.cellPtr
    in
    Html.div
        [ Html.Attributes.style
            ([ ( "position", "absolute" )
             , ( "width", toString pixelWidthInt ++ "px" )
             , ( "height", toString pixelWidthInt ++ "px" )
             , ( "left", toString (x * pixelWidthInt) ++ "px" )
             , ( "top", toString (y * pixelWidthInt) ++ "px" )
             , ( "border", "1px solid black" )
             ]
                ++ (if canPlay then
                        [ ( "background-color", "green" ) ]
                    else
                        []
                   )
            )
        ]
        [ child ]

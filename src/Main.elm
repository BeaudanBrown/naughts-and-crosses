module Main
    exposing
        ( main
        )

import Data.Game exposing (Game)
import Data.PositiveInt as PositiveInt
import Data.StrictlyPositiveInt as StrictlyPositiveInt
import Html exposing (Html)
import View.Game


-- MODEL


type alias Model =
    { game : Game
    }


init : ( Model, Cmd Msg )
init =
    let
        size =
            StrictlyPositiveInt.fromInt 3
                |> Maybe.withDefault StrictlyPositiveInt.one

        depth =
            StrictlyPositiveInt.fromInt 2
                |> Maybe.withDefault StrictlyPositiveInt.one

        game =
            Data.Game.create size depth
    in
    ( { game = game }, Cmd.none )


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        []



-- UPDATE


type Msg
    = Nothing
    | GameMsg View.Game.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Nothing ->
            ( model, Cmd.none )

        GameMsg gameMsg ->
            case gameMsg of
                View.Game.OnClickMsg ptr ->
                    let
                        newGame =
                            Data.Game.makeMove ptr model.game
                                |> Maybe.withDefault model.game

                        newModel =
                            { model | game = newGame }
                    in
                    ( newModel, Cmd.none )



-- VIEW


view : Model -> Html Msg
view model =
    let
        pixelWidth =
            PositiveInt.fromInt 1200
                |> Maybe.withDefault PositiveInt.zero
    in
    View.Game.render pixelWidth model.game
        |> Html.map GameMsg

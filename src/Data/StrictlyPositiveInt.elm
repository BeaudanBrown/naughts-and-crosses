module Data.StrictlyPositiveInt
    exposing
        ( StrictlyPositiveInt
        , fromInt
        , one
        , toInt
        )


type StrictlyPositiveInt
    = StrictlyPositiveInt Int


one : StrictlyPositiveInt
one =
    StrictlyPositiveInt 1


fromInt : Int -> Maybe StrictlyPositiveInt
fromInt x =
    if x < 0 then
        Nothing
    else
        Just (StrictlyPositiveInt x)


toInt : StrictlyPositiveInt -> Int
toInt (StrictlyPositiveInt x) =
    x

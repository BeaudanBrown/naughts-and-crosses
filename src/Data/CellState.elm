module Data.CellState
    exposing
        ( CellState(..)
        )

import Data.Board.Board exposing (Board)


type CellState
    = Naught
    | Cross
    | Blank
    | CellBoard (Board CellState)

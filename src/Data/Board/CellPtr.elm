module Data.Board.CellPtr
    exposing
        ( CellPtr
        , append
        , empty
        , fromList
        , isPredecessor
        , lastCrds
        , toList
        , toString
        , updateLastCrds
        )

import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.PositiveInt as PositiveInt exposing (PositiveInt)
import Data.StrictlyPositiveInt as StrictlyPositiveInt exposing (StrictlyPositiveInt)


type CellPtr
    = CellPtr (List CellCrds)


empty : CellPtr
empty =
    CellPtr []


append : CellCrds -> CellPtr -> CellPtr
append crds (CellPtr xs) =
    CellPtr (xs ++ [ crds ])


toList : CellPtr -> List CellCrds
toList (CellPtr list) =
    list


fromList : List CellCrds -> CellPtr
fromList crds =
    CellPtr crds


lastCrds : CellPtr -> CellCrds
lastCrds (CellPtr list) =
    let
        length =
            List.length list
    in
    list
        |> List.drop (length - 1)
        |> List.head
        |> Maybe.withDefault CellCrds.origin


updateLastCrds : CellPtr -> CellCrds -> CellPtr
updateLastCrds (CellPtr list) newLast =
    let
        length =
            List.length list
    in
    list
        |> List.take (length - 1)
        |> (\newList ->
                newList ++ [ newLast ]
           )
        |> CellPtr


toString : CellPtr -> String
toString (CellPtr list) =
    let
        boardSize =
            StrictlyPositiveInt.fromInt 3
                |> Maybe.withDefault StrictlyPositiveInt.one
    in
    List.foldl
        (\x acc ->
            x
                |> CellCrds.toIdx boardSize
                |> Maybe.withDefault PositiveInt.zero
                |> PositiveInt.toInt
                |> Basics.toString
                |> (++) (acc ++ ", ")
        )
        ""
        list


isPredecessor : CellPtr -> CellPtr -> Bool
isPredecessor (CellPtr preList) (CellPtr childList) =
    case preList of
        [] ->
            True

        x :: xs ->
            case childList of
                [] ->
                    False

                y :: ys ->
                    if x == y then
                        isPredecessor (CellPtr xs) (CellPtr ys)
                    else
                        False

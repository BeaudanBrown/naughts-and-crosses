module Data.Board.Board
    exposing
        ( Board
        , create
        , getCell
        , getSize
        , setCell
        , toList
        , toNonemptyList
        )

import Array exposing (Array)
import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.PositiveInt as PositiveInt
import Data.StrictlyPositiveInt as StrictlyPositiveInt exposing (StrictlyPositiveInt)
import List.Nonempty exposing (Nonempty)


type Board a
    = Board a (Array a)


type alias Size =
    StrictlyPositiveInt


create : Size -> a -> Board a
create boardSize a =
    boardSize
        |> StrictlyPositiveInt.toInt
        |> (\sizeInt -> sizeInt * sizeInt)
        |> (\len -> Array.repeat len a)
        |> Board a


getCell : CellCrds -> Board a -> Maybe a
getCell crds ((Board _ cells) as b) =
    crds
        |> CellCrds.toIdx (getSize b)
        |> Maybe.map PositiveInt.toInt
        |> Maybe.andThen (\idx -> Array.get idx cells)


setCell : CellCrds -> a -> Board a -> Maybe (Board a)
setCell crds a ((Board default cells) as b) =
    crds
        |> CellCrds.toIdx (getSize b)
        |> Maybe.map PositiveInt.toInt
        |> Maybe.map (\idx -> Array.set idx a cells)
        |> Maybe.map (\newCells -> Board default newCells)


getSize : Board a -> Size
getSize (Board _ cells) =
    cells
        |> Array.length
        |> toFloat
        |> sqrt
        |> round
        |> StrictlyPositiveInt.fromInt
        |> Maybe.withDefault StrictlyPositiveInt.one


{-| Private helper. We use a default to avoid returning a Maybe; this works
because we're guaranteed to succeed (this should only ever be called with a
valid idx).
-}
idxToCrds : StrictlyPositiveInt -> CellCrds.Index -> CellCrds
idxToCrds boardSize idx =
    CellCrds.fromIdx boardSize idx
        |> Maybe.withDefault CellCrds.origin


toList : Board a -> List ( CellCrds, a )
toList ((Board _ cells) as b) =
    let
        size =
            getSize b
    in
    cells
        |> Array.toIndexedList
        |> List.map
            (\( idxInt, a ) ->
                let
                    crds =
                        idxInt
                            |> PositiveInt.fromInt
                            |> Maybe.withDefault PositiveInt.zero
                            |> CellCrds.fromIdx size
                            |> Maybe.withDefault CellCrds.origin
                in
                ( crds, a )
            )


toNonemptyList : Board a -> Nonempty ( CellCrds, a )
toNonemptyList ((Board default cells) as b) =
    let
        boardSize =
            getSize b
    in
    Array.indexedMap
        (\idx cell ->
            PositiveInt.fromInt idx
                |> Maybe.withDefault PositiveInt.zero
                |> (\idxInt -> ( idxToCrds boardSize idxInt, cell ))
        )
        cells
        |> Array.toList
        |> List.Nonempty.fromList
        |> Maybe.withDefault
            (List.Nonempty.fromElement ( CellCrds.origin, default ))

module Data.Board.CellCrds
    exposing
        ( CellCrds
        , Index
        , fromIdx
        , fromInts
        , origin
        , toIdx
        , toInts
        , valid
        )

import Data.PositiveInt as PositiveInt exposing (PositiveInt)
import Data.StrictlyPositiveInt as StrictlyPositiveInt exposing (StrictlyPositiveInt)
import Util.Logic as Logic


type alias Index =
    PositiveInt


type CellCrds
    = CellCrds ( PositiveInt, PositiveInt )


origin : CellCrds
origin =
    CellCrds ( PositiveInt.zero, PositiveInt.zero )


fromInts : ( Int, Int ) -> Maybe CellCrds
fromInts ( xInt, yInt ) =
    Logic.tupleIfBothJust
        ( PositiveInt.fromInt xInt, PositiveInt.fromInt yInt )
        |> Maybe.map (\( x, y ) -> CellCrds ( x, y ))


toInts : CellCrds -> ( Int, Int )
toInts (CellCrds ( x, y )) =
    ( PositiveInt.toInt x, PositiveInt.toInt y )


fromIdx : StrictlyPositiveInt -> Index -> Maybe CellCrds
fromIdx boardSize idx =
    let
        sizeInt =
            StrictlyPositiveInt.toInt boardSize

        idxInt =
            PositiveInt.toInt idx

        xInt =
            idxInt // sizeInt

        yInt =
            idxInt % sizeInt
    in
    fromInts ( xInt, yInt )


toIdx : StrictlyPositiveInt -> CellCrds -> Maybe Index
toIdx boardSize crds =
    let
        ( x, y ) =
            toInts crds

        idx =
            (StrictlyPositiveInt.toInt boardSize * x) + y
    in
    Logic.justIf (valid boardSize crds) idx
        |> Maybe.andThen PositiveInt.fromInt


valid : StrictlyPositiveInt -> CellCrds -> Bool
valid boardSize crds =
    let
        ( x, y ) =
            toInts crds

        b =
            StrictlyPositiveInt.toInt boardSize
    in
    x < b && y < b

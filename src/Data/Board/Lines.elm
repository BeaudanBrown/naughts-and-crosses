module Data.Board.Lines
    exposing
        ( Line
        , Lines
        , fromBoard
        , lineToArray
        , toFlatArray
        )

import Array exposing (Array)
import Data.Board.Board as Board exposing (Board)
import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.PositiveInt as PositiveInt
import Data.StrictlyPositiveInt as StrictlyPositiveInt
import List.Nonempty exposing (Nonempty)


type Lines a
    = Lines
        { rows : Array (Line a)
        , cols : Array (Line a)
        , leftDiag : Line a
        , rightDiag : Line a
        }


type Line a
    = Line (Array ( CellCrds, a ))


type alias LineCrds =
    Array CellCrds


fromBoard : Board a -> Lines a
fromBoard board =
    let
        boardSizeInt =
            board
                |> Board.getSize
                |> StrictlyPositiveInt.toInt

        list =
            Board.toNonemptyList board
    in
    Lines
        { rows = toRows boardSizeInt list
        , cols = toCols boardSizeInt list
        , leftDiag = toLeftDiag boardSizeInt list
        , rightDiag = toRightDiag boardSizeInt list
        }


lineToArray : Line a -> Array ( CellCrds, a )
lineToArray (Line arr) =
    arr


toFlatArray : Lines a -> Array (Line a)
toFlatArray (Lines lines) =
    lines.rows
        |> Array.append lines.cols
        |> Array.push lines.leftDiag
        |> Array.push lines.rightDiag


{-| Private helper.
-}
toRows : Int -> Nonempty ( CellCrds, a ) -> Array (Line a)
toRows boardSizeInt list =
    boardSizeInt
        |> crdsRows
        |> Array.map (toLine boardSizeInt list)


{-| Private helper.
-}
toCols : Int -> Nonempty ( CellCrds, a ) -> Array (Line a)
toCols boardSizeInt list =
    boardSizeInt
        |> crdsCols
        |> Array.map (toLine boardSizeInt list)


{-| Private helper.
-}
toLine : Int -> Nonempty ( CellCrds, a ) -> LineCrds -> Line a
toLine boardSizeInt list lineCrds =
    let
        boardSize =
            boardSizeInt
                |> StrictlyPositiveInt.fromInt
                |> Maybe.withDefault StrictlyPositiveInt.one
    in
    lineCrds
        |> Array.map (CellCrds.toIdx boardSize >> Maybe.withDefault PositiveInt.zero)
        |> Array.map
            (\idx -> List.Nonempty.get (PositiveInt.toInt idx) list)
        |> Line


{-| Private helper.
-}
toLeftDiag : Int -> Nonempty ( CellCrds, a ) -> Line a
toLeftDiag boardSizeInt list =
    boardSizeInt
        |> crdsLeftDiag
        |> toLine boardSizeInt list


{-| Private helper.
-}
toRightDiag : Int -> Nonempty ( CellCrds, a ) -> Line a
toRightDiag boardSizeInt list =
    boardSizeInt
        |> crdsRightDiag
        |> toLine boardSizeInt list


{-| Private helper.
-}
crdsRows : Int -> Array LineCrds
crdsRows sizeInt =
    Array.initialize sizeInt (\yInt -> Array.repeat sizeInt yInt)
        |> Array.map crdsRow


{-| Private helper.
-}
crdsRow : Array Int -> LineCrds
crdsRow yInts =
    yInts
        |> Array.indexedMap
            (\xInt yInt ->
                CellCrds.fromInts ( xInt, yInt )
                    |> Maybe.withDefault CellCrds.origin
            )


{-| Private helper.
-}
crdsCols : Int -> Array LineCrds
crdsCols sizeInt =
    Array.initialize sizeInt (\xInt -> Array.repeat sizeInt xInt)
        |> Array.map crdsCol


{-| Private helper.
-}
crdsCol : Array Int -> LineCrds
crdsCol xInts =
    xInts
        |> Array.indexedMap
            (\yInt xInt ->
                CellCrds.fromInts ( xInt, yInt )
                    |> Maybe.withDefault CellCrds.origin
            )


{-| Private helper. Generates the co-ordinates from (0,0) to (size-1, size-1).
-}
crdsLeftDiag : Int -> LineCrds
crdsLeftDiag sizeInt =
    Array.initialize sizeInt
        (\i ->
            CellCrds.fromInts ( i, i )
                |> Maybe.withDefault CellCrds.origin
        )


{-| Private helper. Generates the co-ordinates from (0, size-1) to (size-1, 0).
-}
crdsRightDiag : Int -> LineCrds
crdsRightDiag sizeInt =
    Array.initialize sizeInt
        (\i ->
            CellCrds.fromInts ( i, sizeInt - i - 1 )
                |> Maybe.withDefault CellCrds.origin
        )

module Data.PositiveInt
    exposing
        ( PositiveInt
        , fromInt
        , map
        , toInt
        , zero
        )


type PositiveInt
    = PositiveInt Int


zero : PositiveInt
zero =
    PositiveInt 0


fromInt : Int -> Maybe PositiveInt
fromInt x =
    if x < 0 then
        Nothing
    else
        Just (PositiveInt x)


toInt : PositiveInt -> Int
toInt (PositiveInt x) =
    x


map : (Int -> Int) -> PositiveInt -> Maybe PositiveInt
map f (PositiveInt x) =
    fromInt (f x)

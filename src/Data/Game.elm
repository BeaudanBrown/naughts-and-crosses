module Data.Game
    exposing
        ( BoardState(..)
        , Game
        , Player
        , create
        , getBoard
        , getBoardWinner
        , getPlayableBoard
        , getPlayerToMove
        , makeMove
        , playerToCellState
        )

import Array exposing (Array)
import Data.Board.Board as Board exposing (Board)
import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.Board.CellPtr as CellPtr exposing (CellPtr)
import Data.Board.Lines as Lines
import Data.CellState as CellState exposing (CellState)
import Data.Delta as Delta
import Data.StrictlyPositiveInt as StrictlyPositiveInt exposing (StrictlyPositiveInt)
import Util.Logic as Logic


type BoardState
    = Win Player
    | Draw
    | Undecided


type alias Size =
    StrictlyPositiveInt


type alias Depth =
    StrictlyPositiveInt


type Player
    = Naught
    | Cross


type alias PrivateRecord =
    { board : Board CellState
    , playerToMove : Player
    , moveHistory : List CellPtr
    }


type Game
    = Game PrivateRecord


create : Size -> Depth -> Game
create size depth =
    { board = createBoard size depth
    , playerToMove = Naught
    , moveHistory = []
    }
        |> Game


createBoard : Size -> Depth -> Board CellState
createBoard size depth =
    case StrictlyPositiveInt.toInt depth of
        1 ->
            Board.create size CellState.Blank

        _ ->
            depth
                |> StrictlyPositiveInt.toInt
                |> (\x -> x - 1)
                |> StrictlyPositiveInt.fromInt
                |> Maybe.withDefault StrictlyPositiveInt.one
                |> createBoard size
                |> CellState.CellBoard
                |> Board.create size


makeMove : CellPtr -> Game -> Maybe Game
makeMove cellPtr (Game { board, playerToMove, moveHistory }) =
    board
        |> makeMoveBoard (CellPtr.toList cellPtr) playerToMove
        |> Maybe.map
            (\newBoard ->
                Game
                    { board = newBoard
                    , playerToMove = flipPlayer playerToMove
                    , moveHistory = cellPtr :: moveHistory
                    }
            )


makeMoveBoard : List CellCrds -> Player -> Board CellState -> Maybe (Board CellState)
makeMoveBoard cellCrds playerToMove board =
    cellCrds
        |> Logic.listToMaybe
        |> Maybe.andThen
            (\( crd, crds ) ->
                board
                    |> Board.getCell crd
                    |> Maybe.andThen
                        (\cell ->
                            case cell of
                                CellState.CellBoard childBoard ->
                                    childBoard
                                        |> makeMoveBoard crds playerToMove
                                        |> Maybe.map CellState.CellBoard
                                        |> Maybe.andThen (\newCell -> Board.setCell crd newCell board)

                                CellState.Blank ->
                                    Board.setCell crd (playerToCellState playerToMove) board

                                _ ->
                                    Nothing
                        )
            )


getBoard : Game -> Board CellState
getBoard (Game { board }) =
    board


getPlayerToMove : Game -> Player
getPlayerToMove (Game { playerToMove }) =
    playerToMove


playerToCellState : Player -> CellState
playerToCellState player =
    case player of
        Naught ->
            CellState.Naught

        Cross ->
            CellState.Cross


flipPlayer : Player -> Player
flipPlayer playerToMove =
    case playerToMove of
        Naught ->
            Cross

        Cross ->
            Naught


getBoardWinner : Board CellState -> BoardState
getBoardWinner board =
    board
        |> Lines.fromBoard
        |> Lines.toFlatArray
        |> Array.map lineWinner
        |> Array.foldr
            (\y acc ->
                case acc of
                    Win _ ->
                        acc

                    _ ->
                        if y == Draw then
                            acc
                        else
                            y
            )
            Draw


lineWinner : Lines.Line CellState -> BoardState
lineWinner line =
    line
        |> Lines.lineToArray
        |> Array.map Tuple.second
        |> Array.map getCellWinner
        |> (\x ->
                if hasBlanks x then
                    Undecided
                else
                    getIfAllSame x
                        |> Maybe.withDefault Draw
           )


getIfAllSame : Array BoardState -> Maybe BoardState
getIfAllSame arr =
    Array.foldr
        (\y acc ->
            acc
                |> Maybe.andThen
                    (\x ->
                        if y /= x then
                            Nothing
                        else
                            acc
                    )
        )
        (Array.get 0 arr)
        arr


hasBlanks : Array BoardState -> Bool
hasBlanks arr =
    arr
        |> Array.toList
        |> List.any ((==) Undecided)


getCellWinner : CellState -> BoardState
getCellWinner cell =
    case cell of
        CellState.Naught ->
            Win Naught

        CellState.Cross ->
            Win Cross

        CellState.CellBoard b ->
            getBoardWinner b

        CellState.Blank ->
            Undecided


getPlayableBoard : Game -> CellPtr
getPlayableBoard (Game { board, moveHistory }) =
    let
        boardSize =
            Board.getSize board
    in
    case moveHistory of
        [] ->
            CellPtr.empty

        lastMove :: _ ->
            let
                delta =
                    Delta.fromMove boardSize lastMove
            in
            lastMove
                |> Delta.getDeltaPtr
                |> (\deltaPtr ->
                        deltaPtr
                            |> CellPtr.lastCrds
                            |> Delta.delta boardSize delta
                            |> Maybe.withDefault CellCrds.origin
                            |> CellPtr.updateLastCrds deltaPtr
                   )

module Data.Delta
    exposing
        ( Delta
        , delta
        , fromMove
        , getDeltaPtr
        )

import Data.Board.CellCrds as CellCrds exposing (CellCrds)
import Data.Board.CellPtr as CellPtr exposing (CellPtr)
import Data.StrictlyPositiveInt as StrictlyPositiveInt exposing (StrictlyPositiveInt)


type alias BoardSize =
    StrictlyPositiveInt


type alias Delta =
    ( Int, Int )


fromMove : BoardSize -> CellPtr -> Delta
fromMove boardSize move =
    let
        halfSize =
            boardSize
                |> StrictlyPositiveInt.toInt
                -- Add one for ceiling int rounding
                |> (+) 1
                |> (//) 2
                -- Minus one for doing arithmatic with indicies
                |> (-) 1
    in
    move
        |> CellPtr.lastCrds
        |> CellCrds.toInts
        |> (\( x, y ) ->
                ( x - halfSize, y - halfSize )
           )


{-| Given an int that has had a delta applied to it, wrap it around if
necessary. Relies on the fact that the components of a delta have magnitude 1.
-}
wrap : BoardSize -> Int -> Int
wrap boardSize i =
    let
        maxIdx =
            StrictlyPositiveInt.toInt boardSize
    in
    i % maxIdx


delta : BoardSize -> Delta -> CellCrds -> Maybe CellCrds
delta boardSize ( dx, dy ) currentCrds =
    if not (CellCrds.valid boardSize currentCrds) then
        Nothing
    else
        let
            ( x, y ) =
                CellCrds.toInts currentCrds

            newX =
                wrap boardSize (x + dx)

            newY =
                wrap boardSize (y + dy)
        in
        CellCrds.fromInts ( newX, newY )


getDeltaPtr : CellPtr -> CellPtr
getDeltaPtr lastMove =
    lastMove
        |> CellPtr.toList
        |> (\crdsList ->
                let
                    length =
                        List.length crdsList
                in
                crdsList
                    |> List.take (length - 1)
                    |> CellPtr.fromList
           )

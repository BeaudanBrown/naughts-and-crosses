module Data.PixelWidth
    exposing
        ( PixelWidth
        )

import Data.PositiveInt exposing (PositiveInt)


type alias PixelWidth =
    PositiveInt

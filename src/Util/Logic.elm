module Util.Logic
    exposing
        ( justIf
        , listToMaybe
        , tupleIfBothJust
        )


justIf : Bool -> a -> Maybe a
justIf p a =
    if p then
        Just a
    else
        Nothing


listToMaybe : List a -> Maybe ( a, List a )
listToMaybe list =
    case list of
        x :: xs ->
            Just ( x, xs )

        _ ->
            Nothing


tupleIfBothJust : ( Maybe a, Maybe b ) -> Maybe ( a, b )
tupleIfBothJust ( ma, mb ) =
    ma
        |> Maybe.andThen
            (\a ->
                mb
                    |> Maybe.map (\b -> ( a, b ))
            )
